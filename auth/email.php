<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Rien pour l'instant, mais ça permet de charger le fichier
 * Il faut quand même retourner un tableau vide
 * @return array|bool
 */
function auth_email_dist() {
	return array();
}

/**
 * Terminer l'authentification par email
 * action=auth&arg=email/frite@patate.org/1234jeton
 */
function auth_email_terminer_identifier_login($jeton) {
	include_spip('action/inscrire_auteur');
	include_spip('inc/config');

	$retour = _T('auth:email_jeton_invalide');

	// On récupère les deux infos utiles
	list($jeton, $id_auteur) = explode('#', base64_decode($jeton));

	// Si le jeton existe bien
	if ($auteur = auteur_verifier_jeton($jeton)) {
		// On l'efface pour ne pas pouvoir le réutiliser
		auteur_effacer_jeton($auteur['id_auteur']);
		$retour = $auteur;
	}
	// Si demandé on renvoie un email de connexion et on prévient à l'écran
	elseif (lire_config('auth_email/renvoyer_jeton')) {
		auth_email_notification($id_auteur);
		$retour = _T('auth:email_jeton_renvoyer');
	}

	return $retour;
}

/**
 * Générer l'URL de connexion pour un compte
 */
function auth_email_generer_url($id_auteur, $redirect='') {
	include_spip('action/inscrire_auteur');
	include_spip('inc/actions');
	include_spip('inc/filtres');

	$id_auteur = intval($id_auteur);

	// Par défaut, on redirige vers l'accueil une fois connecté
	if (!$redirect) {
		$redirect = url_de_base();
	}

	// Tant qu'il n'y a pas de datation du jeton, on n'en crée un nouveau seulement si pas déjà un, pour ne pas invalider les URL de connexion non encore utilisées
	if (!$jeton = sql_getfetsel('cookie_oubli', 'spip_auteurs', 'id_auteur ='.$id_auteur)) {
		$jeton = auteur_attribuer_jeton($id_auteur);
	}

	// On ajoute l'id de l'auteur un peu caché
	$jeton = base64_encode($jeton."#$id_auteur");

	// On s'assure que l'action soit toujours lancée côté site public, alors la redirection doit toujours être relative au site public
	if (_DIR_RACINE and strpos($redirect, _DIR_RACINE) === 0) {
		$redirect = preg_replace('%^'._DIR_RACINE.'%', '', $redirect);
	}

	// Forcer pour un id_auteur anonyme puisque justement c'est pour une personne PAS encore connectée
	// et forcer URL publique
	$url = generer_action_auteur('auth', "email/$jeton", $redirect, false, 0, true);

	// On s'assure que la personne soit déconnectée pour utiliser l'action auth en mode id=0
	// on suppose que le cas le plus courant est qu'elle est
	// - soit anonyme (logout marche toujours dans ce cas)
	// - soit était déjà connecté avec le bon compte (dans ce cas ça déco et reco)
	// Si elle était connecté sur le même ordi avec un tout autre compte, d'après le commentaire du code ça affiche un bouton explicite minipres, moins sympa mais ça marchera aussi
	$url = generer_url_action('logout', 'logout=public&url=' . rawurlencode($url), false, true);

	return url_absolue($url);
}

/**
 * Envoie un email avec un lien de connexion pour un compte donné
 *
 */
function auth_email_notification(int $id_auteur, string $redirect='') {
	$envoyer_mail = charger_fonction('envoyer_mail', 'inc/');
	$url = auth_email_generer_url($id_auteur, $redirect);

	if ($email = sql_getfetsel('email', 'spip_auteurs', 'id_auteur = '.$id_auteur)) {
		$envoyer_mail(
			$email,
			_T('auth:email_notification_sujet', array('nom_site' => $GLOBALS['meta']['nom_site'])),
			array(
				'html' => recuperer_fond(
					'emails/texte',
					array(
						'html' => recuperer_fond(
							'notifications/auteur_auth_email',
							array('id_auteur' => $id_auteur, 'url' => $url))
						)
				),
			)
		);
	}

}
