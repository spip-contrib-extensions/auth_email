<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function auth_email_recuperer_fond($flux) {
	if ($flux['args']['fond']=='formulaires/login') {
		include_spip('inc/saisies');
		
		// On ajoutera une méthode par email
		$methode_email = saisies_generer_html(
			array(
				'saisie' => 'fieldset',
				'options' => array(
					'nom' => 'auth_email',
					'onglet' => 'oui',
					'label' => _T('auth:email_methode_label'),
					'inserer_fin' => '<script type="text/javascript">
						;jQuery(function($) {
							// Supprimer les required
							$(".formulaire_login input[required]").removeAttr("required");
						});
					</script>',
				),
				'saisies' => array(
					array(
						'saisie' => 'explication',
						'options' => array(
							'nom' => 'auth_email_explication',
							'texte' => _T('auth:email_methode_explication'),
						),
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'email',
							'label' => _T('ecrire:entree_adresse_email'),
							'defaut' => _request('email'),
						),
						'verifier' => array(
							'type' => 'email',
						),
					),
				),
			),
			$flux['args']['contexte']
		);
		
		// On cherche le vieux fieldset pourri sans aucune classe
		$flux['data']['texte'] = preg_replace('%<fieldset>\s*?<legend>.*?</legend>%ims', '<fieldset><legend>'._T('auth:password_methode_label').'</legend>', $flux['data']['texte']);
		$flux['data']['texte'] = preg_replace('%<fieldset>.*?</fieldset>%ims', $methode_email . '<div class="auth_password fieldset fieldset_onglet">$0</div>', $flux['data']['texte']);
	}
	
	return $flux;
}

function auth_email_formulaire_verifier($flux) {
	if ($flux['args']['form'] == 'login') {
		// Si on a rempli le champ par email et PAS le login classique
		if ($email = _request('email') and !_request('var_login')) {
			include_spip('auth/email');
			include_spip('inc/filtres');
			
			// On vide toujours les erreurs des autres champs
			unset($flux['data']['var_login']);
			unset($flux['data']['password']);
			
			// Avec cette méthode, on ne connecte rien directement, on ne va pas au bout du processus
			// on envoie un email avec un lien pour finir la connexion et donc on ne va pas jusqu'au traiter()
			if (
				email_valide($email)
				and $id_auteur = sql_getfetsel(
					'id_auteur',
					'spip_auteurs',
					array('email = '.sql_quote($email), 'statut != "5poubelle"')
				)
			) {
				// Après connexion, on redirige vers la page d'où on a demandé la connexion
				auth_email_notification($id_auteur, $flux['args']['args'][0] ?: self());
			}
			
			// Un message générique, qui ne donne pas d'information sur l'existence ou pas du compte
			$flux['data']['message_erreur'] = '';
			$flux['data']['message_ok'] = _T('auth:email_message_ok');
		}
	}
	
	return $flux;
}
