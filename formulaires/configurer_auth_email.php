<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_configurer_auth_email_saisies_dist() {
	$saisies = array(
		array(
			'saisie' => 'case',
			'options' => array(
				'nom' => 'renvoyer_jeton',
				'label_case' => _T('auth:email_configurer_renvoyer_jeton_label_case'),
				'conteneur_class' => 'pleine_largeur',
			),
		),
	);
	
	return $saisies;
}
