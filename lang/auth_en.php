<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'email_configurer_renvoyer_jeton_label_case' => 'When a token is no longer valid, immediately resend an email with a new link.',
	'email_configurer_titre' => 'Set up email login.',
	'email_jeton_invalide' => 'This login token is not or no longer valid.',
	'email_jeton_renvoyer' => 'This login token is not or no longer valid. A new link has been sent to your email.',
	'email_message_ok' => 'If this address corresponds to an account, you will receive an email with a link.',
	'email_methode_explication' => 'Provide the address associated with your account, and we will send you a link.',
	'email_methode_label' => 'Email login.',
	'email_notification_message' => "By clicking on this link, you will be logged in to the site <strong>@nom_site@</strong>.",
	'email_notification_sujet' => 'Log in to the site @nom_site@',
	'password_methode_label' => 'Log in with a password',
);
