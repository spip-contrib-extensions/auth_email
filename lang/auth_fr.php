<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'email_configurer_renvoyer_jeton_label_case' => 'Lorsqu’un jeton n’est plus valide, renvoyer immédiatement un email avec un nouveau lien de connexion',
	'email_configurer_titre' => 'Configurer l’authentification par email',
	'email_jeton_invalide' => 'Ce jeton de connexion n’est pas ou plus valide.',
	'email_jeton_renvoyer' => 'Ce jeton de connexion n’est pas ou plus valide. Un nouveau lien de connexion vient d’être envoyé à votre adresse.',
	'email_message_ok' => 'Si cette adresse correspond à un compte, vous allez recevoir un email avec un lien de connexion.',
	'email_methode_explication' => 'Renseignez l’adresse associée à votre compte, un lien de connexion vous sera envoyé.',
	'email_methode_label' => 'Connexion par email',
	'email_notification_message' => "En cliquant sur ce lien, vous serez connecté au site <strong>@nom_site@</strong>.",
	'email_notification_sujet' => 'Connexion au site @nom_site@',
	'password_methode_label' => 'Connexion par mot de passe',
);
