<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'auth_email_description' => 'Ce plugin ajoute un mode de connexion depuis un lien temporaire envoyé par email. Il est possible de générer ces liens de connexion depuis une fonction fournie afin de les utiliser dans n’importe quelle autre notification.',
	'auth_email_nom' => 'Authentification par email',
	'auth_email_slogan' => 'Permet de se connecter par un lien envoyé par email'
);
